import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postCreatedDate;
  @Input() postLoveIts: number


  constructor() { }

  ngOnInit() {
  }

  public isLoved() {
    if (this.postLoveIts > 0) {
      return true;
    } else if (this.postLoveIts < 0) {
      return false;
    }
  }

  public hasVotes() {
    if (this.postLoveIts === 0) {
      return false;
    } else {
      return true;
    }
  }

  public onLoveIt() {
    this.postLoveIts ++;
  }

  public onDontLoveIt() {
    this.postLoveIts --;
  }

}
